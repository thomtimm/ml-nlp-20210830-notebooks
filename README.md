# From Machine Learning to Text Generation

author
: Thomas Timmermann

date
: August 30, 2021



## Option 1: Access via binder

The notebooks can be accessed at

https://mybinder.org/v2/gl/thomtimm%2Fml-nlp-20210830-notebooks/master

but beware mybinder's memory limit of 2GB.

## Option 2: Running locally within a virtual environment

This requires ``python3`` and ``pip3``:

1. Create a [virtual environment](https://docs.python.org/3/library/venv.html):

   ``` sh
   python3 -m venv .venv
   ```

2. Activate virtual environment using

   ```sh
   source .venv/bin/activate
   ```
   on Mac or Linux, or
   
   ```sh
   .venv\Scripts\activate.bat
   ```
   
   on normal Windows shell or 
   
   ```sh
   .venv\Scripts\Activate.ps1
   ```
   
   on Windows PowerShell. See [virtual environment](https://docs.python.org/3/library/venv.html) in case of trouble.

3. Install python dependencies using [pip](https://pip.pypa.io/en/stable/):

   ``` sh
   pip3 install -r requirements.txt
   ```

4. Start [jupyter](https://jupyter.org/):

   ``` sh
   jupyter lab notebooks
   ```


Steps 1 and 3 need to be done only once. Thereafter, steps 2+4 suffice.

## Option 3: Running in docker

This requires [docker](https://www.docker.com/):

1. Build the docker image with

   ``` sh
   docker build -t ml-nlp-workshop -f workshop.Dockerfile .
   ```
  
2. Run it with

   ``` sh
   docker run --mount "type=bind,src={WORKSHOP_NOTEBOOKS_DIRECTORY},dst=/notebooks" --rm -it  -p 8888:8888  ml-nlp-workshop
   ```
   
   where ``WORKSHOP_NOTEBOOKS_DIRECTORY`` is the full path to the notebooks in this repository.




