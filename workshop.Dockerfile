FROM python:3.8-buster

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
RUN mkdir /notebooks
CMD jupyter lab --allow-root --no-browser --ip 0.0.0.0 --port 8888 /notebooks
