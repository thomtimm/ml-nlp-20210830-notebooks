{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "jupyter": {
     "outputs_hidden": false
    },
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "# Hands-On: Classifying poems as character sequences with a CNN"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this notebook we're going to classify poems of Goethe, Heine and Tucholsky, taken from the dataset [Deutscher Lyrik-Korpus](https://github.com/thomasnikolaushaider/DLK) presented at\n",
    "\n",
    "> Haider, T. and Eger, S.  \n",
    "> **_Semantic Change and Emerging Tropes In a Large Corpus of New High German Poetry._**  \n",
    "> Proceedings of the 1st International Workshop on Computational Approaches to Historical Language Change. 2019.\n",
    "\n",
    "Our selection from the dataset is stored as compressed JSON. We use `pandas` to load it as a `pandas.DataFrame` table:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "EXTRACT = 'data/poems/selected_poems.json.bz2'\n",
    "poems = pd.read_json(EXTRACT, compression='infer')\n",
    "poems.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at the distribution of authors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poems['author'].value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lets look at how long the texts are:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poems['len'] = poems['text'].apply(len)\n",
    "\n",
    "poems.boxplot('len', by='author')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How can we make this plot more informative?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import altair as alt\n",
    "\n",
    "alt.Chart(poems, width=200).mark_boxplot().encode(x='author', y=alt.Y('len', scale=alt.Scale(type='log')))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparing the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Encoding poems as character sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We treat the poems as sequences of characters and first determine the set of all characters appearing in the poems:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "used_alphabet = set().union(*poems['text'].apply(set))\n",
    "''.join(sorted(used_alphabet))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The poems contain some strange characters, which we would like to filter out. So, we fix an alphabet to use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ALPHABET = 'abcdefghijklmnopqrstuvwxyzäöüßABCDEFGHIKLMNOPQRSTUVWXZYÄÖÜ .,;:!?-()\"\\'\\n'\n",
    "len(ALPHABET)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now transform each poem as follows: each character is replaced by its index in `alphabet`, starting with 1, or by 0 if it is not contained in the alphabet. Moreover, we cut each poem down to the first 1000 characters and pad with a special sign to obtain sequences of a fixed length."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "char_index = {char: index + 1 for index, char in enumerate(ALPHABET)}\n",
    "\n",
    "def index_characters(text):\n",
    "    return [char_index.get(char, 0) for char in text]\n",
    "                                              \n",
    "poems['characters'] = poems.text.apply(index_characters)\n",
    "poems[['text', 'characters']].head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we apply a one-hot-encoding:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "eye = np.eye(len(ALPHABET))\n",
    "zeros = np.zeros((1, len(ALPHABET)))\n",
    "codes = np.vstack([zeros, eye])\n",
    "codes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poems['characters_ohe'] = poems.characters.apply(lambda chars: codes[chars])\n",
    "poems.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poems.loc[43471, 'characters_ohe']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we want to stack the matrices obtained for the poems together. For this, we use a convenience function of keras:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.preprocessing.sequence import pad_sequences\n",
    "\n",
    "FIXED_LEN = 1000\n",
    "X = pad_sequences(poems.characters_ohe, maxlen=FIXED_LEN)\n",
    "X.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Encoding the authors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we want to encode the labels, that is, the authors. We could do that as before, or use pandas' convenience function `get_dummies`..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "authors_ohe = pd.get_dummies(poems.author)\n",
    "authors_ohe.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We access the raw matrix as the `value` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "y = authors_ohe.values\n",
    "y[:5]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Shuffle and split the dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we need to shuffle and split our data. For the moment, we do this by hand as follows: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def train_test_split(X,y,ratio=0.7):\n",
    "    total = X.shape[0]\n",
    "    indices = np.random.permutation(total)\n",
    "    pos = int(0.7 * total)\n",
    "    train_indices, test_indices = indices[:pos], indices[pos:]\n",
    "    return (X[train_indices], y[train_indices]), (X[test_indices], y[test_indices])\n",
    "\n",
    "(X_train, y_train), (X_val, y_val) = train_test_split(X,y)\n",
    "X_train.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training a neural network for classification"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### What about dense layers as before?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to train a neural network to learn to classify the author of a poem. Let's try a similar network as for the mnist task:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from tensorflow.keras.models import Sequential\n",
    "from tensorflow.keras.layers import Dense, Dropout, Flatten"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def build_model():\n",
    "    return Sequential([\n",
    "        Flatten(input_shape=(FIXED_LEN, 71)),\n",
    "        Dense(128, activation='relu'),\n",
    "        Dropout(0.3),\n",
    "        Dense(3, activation='softmax')\n",
    "    ])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mlflow\n",
    "\n",
    "def train_model(model, epochs=10, batch_size=32):\n",
    "    mlflow.set_experiment('poems-character')\n",
    "    with mlflow.start_run():\n",
    "        mlflow.keras.autolog()\n",
    "        model.compile(loss='categorical_crossentropy', metrics=['accuracy'], optimizer='Adam')\n",
    "        model.summary()\n",
    "        history = model.fit(X_train,y_train, epochs=epochs, batch_size=batch_size, validation_split=0.2)\n",
    "    return model, pd.DataFrame(history.history)\n",
    "    \n",
    "model = build_model()\n",
    "model, history = train_model(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is not going to get us very far... We observe an extreme form of overfitting."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A better try: convolutional layers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now train a convolutional neural network consisting of\n",
    "\n",
    "- a stack of **convolutional layers** for pattern extraction and \n",
    "- a **dense layer** for classification."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "((3 * 71) +1) * 64"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(128 + 1) * 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.layers import Conv1D, GlobalMaxPooling1D, BatchNormalization\n",
    "\n",
    "def build_model():\n",
    "    return Sequential([\n",
    "        Conv1D(64, kernel_size=3, strides=2, activation='relu', input_shape=(None, len(ALPHABET))),\n",
    "        Conv1D(128, kernel_size=3, strides=2, activation='relu'),\n",
    "        GlobalMaxPooling1D(),\n",
    "        Dense(3, activation='softmax')\n",
    "    ])\n",
    "\n",
    "model, history = train_model(build_model())\n",
    "history"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us visualize the training history:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "import seaborn as sns\n",
    "sns.set()\n",
    "\n",
    "def plot_history(history):\n",
    "    _, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))\n",
    "    history[['loss', 'val_loss']].plot.line(ax=ax1)\n",
    "    history[['accuracy', 'val_accuracy']].plot.line(ax=ax2)\n",
    "    \n",
    "plot_history(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Aside: Altair and the grammar of graphics again"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the same visualization with [altair](https://altair-viz.github.io/):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import altair as alt\n",
    "\n",
    "history_long = history.reset_index().melt(id_vars=['index'])\n",
    "history_long['type'] = history_long['variable'].str.endswith('loss')\n",
    "history_long.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "alt.Chart(history_long).mark_line().encode(x='index', y='value', color='variable', column='type', tooltip=list(history_long.columns)) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let us evaluate the trained model on the validation data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def validate(model):\n",
    "    y_pred = np.argmax(model.predict(X_val), axis=1)\n",
    "    y_true = np.argmax(y_val, axis=1)\n",
    "    return y_true, y_pred\n",
    "\n",
    "y_true, y_pred = validate(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A confusion matrix gives a useful view on the validation result. One way to get this matrix is the function `pd.crosstab`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def confusion(y_true, y_pred):\n",
    "    confusion_matrix = pd.crosstab(y_true, y_pred)\n",
    "    confusion_matrix.index = authors_ohe.columns\n",
    "    confusion_matrix.columns = authors_ohe.columns\n",
    "    return confusion_matrix\n",
    "\n",
    "confusion(y_true, y_pred)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise: Training an embedding layer for characters"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instead of a one-hot encoding, we can train more dense embeddings for the characters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = pad_sequences(poems.characters, maxlen=FIXED_LEN)\n",
    "(X_train, y_train), (X_val, y_val) = train_val_split(X,y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.layers import Embedding, MaxPooling1D\n",
    "\n",
    "def build_model():\n",
    "    return Sequential([\n",
    "        Embedding(len(ALPHABET) + 1, 24, input_shape=(FIXED_LEN,)),\n",
    "        Conv1D(64, kernel_size=3, strides=1, activation='relu'),\n",
    "        Conv1D(128, kernel_size=3, strides=1, activation='relu'),\n",
    "        GlobalMaxPooling1D(),\n",
    "        Dense(128, activation='relu'),\n",
    "        Dropout(0.2),\n",
    "        Dense(3, activation='softmax')\n",
    "    ])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model, _ = train_model(build_model(), epochs=10)\n",
    "confusion(*validate(model))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "name": "Untitled3.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
