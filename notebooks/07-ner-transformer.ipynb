{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Custom NER with Transformers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We load the data and determine the set of all tags as before:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "MAX_LEN = 50\n",
    "\n",
    "def load_data(filename: str):\n",
    "    samples = []\n",
    "    with open(filename, 'r') as file:\n",
    "        sentence = []\n",
    "        for line in file:\n",
    "            line = line[:-1] # trim newline\n",
    "            if line:\n",
    "                token, full_tag = line.split()\n",
    "                tag = full_tag.split('-')[-1]\n",
    "                sentence.append((token, tag))\n",
    "            else:\n",
    "                samples.append(sentence[:MAX_LEN])\n",
    "                sentence = []\n",
    "    return samples\n",
    "\n",
    "TRAIN_FILE = 'data/legal/01_raw/bag.conll'\n",
    "train_samples = load_data(TRAIN_FILE)\n",
    "\n",
    "VAL_FILE = 'data/legal/01_raw/bgh.conll'\n",
    "val_samples = load_data(VAL_FILE)\n",
    "\n",
    "schema = ['_'] + sorted({tag\n",
    "                         for sentence in train_samples + val_samples\n",
    "                         for _, tag in sentence})\n",
    "print(' '.join(schema))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The data is now stored as a list of token-tag-pairs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "train_samples[3][:14]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Loading the BERT transformer model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The library [huggingface/transformers](https://huggingface.co/transformers) provides many\n",
    "- **pre-trained** state-of-the-art **transformer models** like BERT, GPT-2 and others,\n",
    "- model **wrappers for downstream tasks** like classification, named entity recognition, summarization,\n",
    "- ways to use these models, e.g. in end-to-end pipelines or via TensorFlow or PyTorch.\n",
    "\n",
    "We shall use\n",
    "\n",
    "- a **BERT transformer** pre-trained on German,\n",
    "- wrapped for **token classification**\n",
    "- in form of a TensorFlow model.\n",
    "\n",
    "Like most of the transformer models, BERT is quite large and loading it may take some time:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from transformers import AutoConfig, TFAutoModelForTokenClassification, AutoTokenizer\n",
    "import tensorflow as tf\n",
    "\n",
    "MODEL_NAME = 'bert-base-german-cased' # another option would be 'german-nlp-group/electra-base-german-uncased' \n",
    "\n",
    "def build_model(extra_metrics=[]):\n",
    "    tokenizer = AutoTokenizer.from_pretrained(MODEL_NAME)\n",
    "    config = AutoConfig.from_pretrained(MODEL_NAME, num_labels=len(schema))\n",
    "    model = TFAutoModelForTokenClassification.from_pretrained(MODEL_NAME, config=config)\n",
    "    optimizer = tf.keras.optimizers.Adam(lr=0.00001)\n",
    "    loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)\n",
    "    model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'] + extra_metrics)\n",
    "    return tokenizer, model\n",
    "\n",
    "tokenizer, model = build_model()\n",
    "model.summary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We obtain an honest Keras model. Alternatively, we could have used the PyTorch interface of huggingface transformers."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preprocessing\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The transformer models are usually pre-trained with **sub-word-level tokenizers**. \n",
    "\n",
    "*The same tokenizers need to be used for the preprocessing.* That's why we loaded it together with the Bert model.\n",
    "\n",
    "The tokenizer used by BERT will treat most common words as one token, but may split rare words into word pieces to avoid the out-of-vocabulary problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "for utterance in ['Das', 'Das ist', 'Das ist eine hoch interessante Frage', 'Das ist eine hochinteressante Frage']:\n",
    "    print(tokenizer(utterance))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Accordingly, we need to adjust the token label. We do so using the function `tokenize_sample` below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def tokenize_sample(sample):\n",
    "    seq = [(subtoken, tag) for token, tag in sample for subtoken in tokenizer(token)['input_ids'][1:-1]]\n",
    "    return [(3, 'O')] + seq + [(4, 'O')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For our samples, the length of the subword sequence will usually be longer than that of the corresponding word sequences:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "print(len(train_samples[1]))\n",
    "print(len(tokenize_sample(train_samples[1])))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now apply the subword-level tokenization to our samples. This may take a few moments:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from tqdm import tqdm\n",
    "\n",
    "def preprocess(samples):\n",
    "    tag_index = {tag: i for i, tag in enumerate(schema)}\n",
    "    tokenized_samples = list(tqdm(map(tokenize_sample, samples)))\n",
    "    max_len = max(map(len, tokenized_samples))\n",
    "    X = np.zeros((len(samples), max_len), dtype=np.int32)\n",
    "    y = np.zeros((len(samples), max_len), dtype=np.int32)\n",
    "    for i, sentence in enumerate(tokenized_samples):\n",
    "        for j, (subtoken_id, tag) in enumerate(sentence):\n",
    "            X[i, j] = subtoken_id\n",
    "            y[i,j] = tag_index[tag]\n",
    "    return X, y\n",
    "\n",
    "X_train, y_train = preprocess(train_samples)\n",
    "X_val, y_val = preprocess(val_samples)\n",
    "X_train.shape, X_val.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can train the model just as we trained the bi-LSTM network, but now we need more resources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "def train(model, nr_samples=-1, epochs=1, batch_size=16):\n",
    "    history = model.fit(tf.constant(X_train[:nr_samples]), tf.constant(y_train[:nr_samples]),\n",
    "                        validation_split=0.2, epochs=epochs, batch_size=batch_size)\n",
    "    return model, pd.DataFrame(history.history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's see whether everything works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "_, model = build_model()\n",
    "model, history = train(model, nr_samples=100, epochs=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This looks promising and resource-hungry.\n",
    "\n",
    "To get more insight into the training process, let us track, for each tag, the accuracy for the one-vs-all classification:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import tensorflow as tf\n",
    "\n",
    "def build_tag_accuracy(index, name):\n",
    "    def tag_accuracy(y_true, y_pred):\n",
    "        y_pred_index = tf.math.argmax(y_pred, axis=-1)\n",
    "        y_t_b = y_true == index\n",
    "        y_p_b = y_pred_index == index\n",
    "        return tf.reduce_mean(tf.where(y_t_b == y_p_b, 1.0, 0.0))\n",
    "\n",
    "    tag_accuracy.__name__ = 'accuracy_' + name\n",
    "    return tag_accuracy\n",
    "\n",
    "tags_metrics = [build_tag_accuracy(index, name) for index, name  in enumerate(schema)]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "_, model = build_model(tags_metrics)\n",
    "model, history = train(model, nr_samples=20, epochs=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before we take more time to train the model on real data, let us see how to apply and evaluate the model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "sns.set()\n",
    "\n",
    "def plot_history(history):\n",
    "    _, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))\n",
    "    history[['loss', 'val_loss']].plot.line(ax=ax1)\n",
    "    history[['accuracy', 'val_accuracy']].plot.line(ax=ax2)\n",
    "\n",
    "plot_history(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model can be applied as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "sample_index = 5\n",
    "probs = model.predict(X_val[sample_index: sample_index + 1])[0][0]\n",
    "preds = np.argmax(probs, axis=-1)\n",
    "probs.shape, preds.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now comes an important point:\n",
    "\n",
    "To compare the results obtained with BERT to other approaches, we should evaluate the predictions \n",
    "\n",
    "- not on the level of the subword token produced by our model-coupled tokenizer\n",
    "- but on the level of the words as they appear in the dataset!\n",
    "\n",
    "Remember that a long and rare word that comes with one tag in the dataset might be split up into several token by the tokenizer and then each of these token will be tagged separately by the BERT model. We need to collect all these tags and decide, e.g. by a majority vote, which of the tags should be chosen for the entire token."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the majority vote, we use NumPy as follows"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def majority_vote(votes):\n",
    "    return np.argmax(np.histogram(votes, bins=len(schema), range=(0, len(schema)))[0])\n",
    "\n",
    "majority_vote(np.array([1,2,1,0,3,1,4]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we need to align the predictions: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def align_predictions(sample, predictions):\n",
    "    results = []\n",
    "    i = 1\n",
    "    for token, y_true in sample:\n",
    "        nr_subtoken = len(tokenizer(token)['input_ids']) -2 # beware of start- and end-token\n",
    "        votes = predictions[i:i+nr_subtoken]\n",
    "        i += nr_subtoken\n",
    "        y_pred = schema[majority_vote(votes)]\n",
    "        y_pred_sub = ''.join(schema[p] for p in votes)\n",
    "        results.append((token, y_true, y_pred, y_pred_sub))\n",
    "    return results\n",
    "\n",
    "pd.DataFrame(align_predictions(val_samples[sample_index], preds)[:20], columns= ['Token', 'Truth', 'Prediction', 'Sub-Predictions'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now predict on the validation set as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def predict(model, nr_val_samples=-1):\n",
    "    y_probs = model.predict(X_val[:nr_val_samples])[0]\n",
    "    y_preds = np.argmax(y_probs, axis=-1)\n",
    "    return [align_predictions(sample, predictions) for sample, predictions in zip(val_samples, y_preds)]\n",
    "\n",
    "predictions = predict(model, nr_val_samples=10)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we evaluate the predictions on the level of token as a one-vs-all classification problem:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from sklearn.metrics import classification_report\n",
    "\n",
    "def evaluate(predictions):\n",
    "    y_t = [pos[1] for sentence in predictions for pos in sentence]\n",
    "    y_p = [pos[2] for sentence in predictions for pos in sentence]\n",
    "    report = classification_report(y_t, y_p, output_dict=True)\n",
    "    return pd.DataFrame.from_dict(report).transpose().reset_index()\n",
    "\n",
    "scores = evaluate(predictions)\n",
    "print(scores)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "name": "03_transformers.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
