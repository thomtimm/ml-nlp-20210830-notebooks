
PYTHON=python3

.PHONY: pip-compile pip-install pip-update install jupyter clean build-docker run-docker

ACTIVATE_VENV = source .venv/bin/activate
#
# GIT_NAMESPACE=git@gitlab.com:thomtimm
GIT_NAMESPACE=git@gitlab.com:thomtimm
GIT_REMOTE=$(GIT_NAMESPACE)/ml-nlp-20210830-notebooks.git

init:
	# git init && git stage . && git commit -m "cookiecutting"  &&
	git push --set-upstream $(GIT_REMOTE) master && \
	git remote add origin $(GIT_REMOTE)

clean:

install-venv:
	$(PYTHON) -m venv .venv && $(ACTIVATE_VENV) && pip3 install --upgrade pip && pip3 install pip-tools

pip-install:
	$(ACTIVATE_VENV) && pip install -r requirements.txt

install: install-venv pip-install install-extensions

pip-compile:
	$(ACTIVATE_VENV) && pip-compile --verbose requirements.in > requirements.txt

pip-update: pip-compile
	$(ACTIVATE_VENV) && pip-sync requirements.txt

jupyter:
	$(ACTIVATE_VENV) && jupyter lab .

mlflow:
	$(ACTIVATE_VENV) && cd notebooks && mlflow ui

TAG = ml-nlp-workshop
PORT = 8888
RUNTIME= #  either '--runtime nvidia' or ''

build-docker:
	docker build -t $(TAG) -f workshop.Dockerfile .

run-docker:
	docker run --mount "type=bind,src=`pwd`,dst=/notebooks" $(RUNTIME) --rm -it  -p $(PORT):8888  $(TAG)
